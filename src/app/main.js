import React from 'react'
import { render } from 'react-dom'
import CredevoApp from './CredevoApp'

render(
    <CredevoApp />,
    document.getElementById('container')
);