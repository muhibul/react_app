/* Deliberatly exposing all of them in global name space */
var h = React.createElement;

var rModal =  React.createClass({
    render: function(){
        return (
            React.createElement('div', {'className' : 'modal fade', 'id' : 'myModal'}, 
                React.createElement('div', {'className' : 'modal-dialog'}, 
                    React.createElement(rModalContent)
                )
            )
        )
    }
});

var rModalContent = React.createClass({
    render: function(){
        return (
            React.createElement('div',
                {
                    'className' : 'modal-content'
                }, 
                React.createElement(rModalHeader),
                React.createElement(rModalBody),
                React.createElement(rModalFooter)
            )
        )
    }
});

var rModalHeader =  React.createClass({
    render: function(){
        return (
            React.createElement('div', {'className' : 'modal-header'}, 
                React.createElement('button', {'className' : 'close'}, '&times;'),
                React.createElement('h4', {'className' : 'modal-title'}, 'Modal Header')
            )
        )
    }
});

var rModalBody =  React.createClass({
    render: function(){
        return (
            React.createElement('div', {'className' : 'modal-body'}, 
                React.createElement('p', null, 'Hello World')
            )
        )
    }
});

var rModalFooter =  React.createClass({
    render: function(){
        return (
            React.createElement('div', {'className' : 'modal-footer'}, 
                React.createElement('button', {'className' : 'btn btn-default'}, 'Close')
            )
        )
    }
});