import React from 'react'
import {render} from 'react-dom'

class CredevoApp extends React.Component {
    constructor(props) {
        super(props);
    }

    handleClick(props) {
        console.log(props);
        //$('#myModal').modal('show');
    }

    render() {
        return(
            <div>
                <button className="btn btn-info btn-lg" onClick={this.handleClick}>Open Modal</button>
            </div>
        );
    }

}

export default CredevoApp