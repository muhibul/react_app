## Step 1: Make sure Node.js is installed globally

## Step 2.1: If there is no package.json file exists , execute following commands:
```
$ npm init
$ npm install react react-dom react-bootstrap webpack --save
$ npm install babel-core babel-loader babel-preset-es2015 babel-preset-react --save-dev
$ npm install react-script-loader --save
```

## Step 2.2: If package.json already exists, execute following command:
```
$ npm install
```

## Step 3: Execute following command and access the page in web-browser
```
$ npm run webpack
```


### Important links to follow:
[php, react.js and laravel] https://codingdash.com/post/php-with-react-js-end-to-end/